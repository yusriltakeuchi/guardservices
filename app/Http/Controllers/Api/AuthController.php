<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller; 
use App\User;
use Auth;
class AuthController extends Controller
{
    public function register(Request $request)
    {
        $request->validate([
            'username' => 'required|max:40|unique:users',
            'email' => 'required|unique:users',
            'password' => 'required|confirmed',
            'name' => 'required',
            'address' => 'required',
            'phone' => 'required|min:10'
        ]);

        $user = User::create([
            'username' => $request->username,
            'email' => $request->email,
            'password' => bcrypt($request->password),
        ]);

        if ($user) {
            $user->profile()->create([
                'name' => $request->name,
                'address' => $request->address,
                'phone' => $request->phone
            ]);

            $token = $user->createToken('authToken')->accessToken;
            return response()->json([
                'user' => $user,
                'access_token' => $token
            ]);
        }
       
    }

    public function login(Request $request)
    {
        $request->validate([
            'username' => 'required|string',
            'password' => 'required|string'
        ]);

        $credentials = request(['username', 'password']);
        if (!Auth::attempt($credentials)) {
            return response()->json([
                'status' => '401',
                'message' => 'Invalid credentials'
            ], 401);
        }

        $token = Auth()->user()->createToken('authToken')->accessToken;
        return response()->json([
            'user' => Auth()->user(),
            'access_token' => $token
        ], 200);
    }
}
