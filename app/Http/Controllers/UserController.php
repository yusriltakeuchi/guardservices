<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;
class UserController extends Controller
{
    public function getuser(Request $request, $id)
    {
        //Get user id from passport token
        $user_id = $request->user()->id;

        //Validate if user token is the id to search
        if ($id == $user_id) {
            $user2 = DB::select('select u.id, u.username, u.email, p.name, p.address, p.phone from users u inner join profile p on u.id = p.id where u.id = ' . $id);
            $user = User::with('profile')->find($id);
            if ($user) {
                return response()->json([
                    'status' => 200,
                    'message' => 'Successfully get users',
                    'data' => $user
                ], 200);
            } else {
                return response()->json([
                    'status' => 404,
                    'message' => 'Users not found',
                ], 404);
            }
            
        } else {
            return response()->json([
                'status' => 401,
                'message' => 'Access Denied'
            ], 401);
        }
    }
}
